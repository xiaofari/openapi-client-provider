package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayResponse;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/4/18 11:38
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.CERT_TWO_ELEMENTS_VERIFY,type = ApiMessageType.Response)
public class YipayCertTwoElementsVerifyResponse extends YipayResponse {

    /**
     * 结果标识
     * 1：验证一致
     * 2：验证不一致
     * 3：公安库中无此号
     */
    private String stat;

    /**
     * 结果描述
     */
    private String desc;
}
