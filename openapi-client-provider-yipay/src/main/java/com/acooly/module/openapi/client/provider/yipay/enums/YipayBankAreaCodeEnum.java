/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.yipay.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by liubin@prosysoft.com on 2017/9/27.
 */
public enum YipayBankAreaCodeEnum implements Messageable {

    BANK_AREA_CODE_110000("110000","北京"),
    BANK_AREA_CODE_120000("120000","天津"),
    BANK_AREA_CODE_130000("130000","河北"),
    BANK_AREA_CODE_140000("140000","山西"),
    BANK_AREA_CODE_150000("150000","内蒙"),
    BANK_AREA_CODE_210000("210000","辽宁"),
    BANK_AREA_CODE_220000("220000","吉林"),
    BANK_AREA_CODE_230000("230000","黑龙"),
    BANK_AREA_CODE_310000("310000","上海"),
    BANK_AREA_CODE_320000("320000","江苏"),
    BANK_AREA_CODE_330000("330000","浙江"),
    BANK_AREA_CODE_340000("340000","安徽"),
    BANK_AREA_CODE_350000("350000","福建"),
    BANK_AREA_CODE_360000("360000","江西"),
    BANK_AREA_CODE_370000("370000","山东"),
    BANK_AREA_CODE_410000("410000","河南"),
    BANK_AREA_CODE_420000("420000","湖北"),
    BANK_AREA_CODE_430000("430000","湖南"),
    BANK_AREA_CODE_440000("440000","广东"),
    BANK_AREA_CODE_450000("450000","广西"),
    BANK_AREA_CODE_460000("460000","海南"),
    BANK_AREA_CODE_500000("500000","重庆"),
    BANK_AREA_CODE_510000("510000","四川"),
    BANK_AREA_CODE_520000("520000","贵州"),
    BANK_AREA_CODE_530000("530000","云南"),
    BANK_AREA_CODE_540000("540000","西藏"),
    BANK_AREA_CODE_610000("610000","陕西"),
    BANK_AREA_CODE_620000("620000","甘肃"),
    BANK_AREA_CODE_630000("630000","青海"),
    BANK_AREA_CODE_640000("640000","宁夏"),
    BANK_AREA_CODE_650000("650000","新疆"),
    BANK_AREA_CODE_710000("710000","台湾"),
    BANK_AREA_CODE_810000("810000","香港"),
    BANK_AREA_CODE_820000("820000","澳门"),;

    private final String code;
    private final String message;

    private YipayBankAreaCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (YipayBankAreaCodeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static YipayBankAreaCodeEnum find(String code) {
        for (YipayBankAreaCodeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("FudianAuditStatusEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<YipayBankAreaCodeEnum> getAll() {
        List<YipayBankAreaCodeEnum> list = new ArrayList<YipayBankAreaCodeEnum>();
        for (YipayBankAreaCodeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (YipayBankAreaCodeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
