package com.acooly.module.openapi.client.provider.hx.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.hx.domain.HxApiMsgInfo;
import com.acooly.module.openapi.client.provider.hx.domain.HxRequest;
import com.acooly.module.openapi.client.provider.hx.enums.HxServiceEnum;
import com.acooly.module.openapi.client.provider.hx.message.xStream.netBankPayQuery.request.OrderQueryReq;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("Ips")
@HxApiMsgInfo(service = HxServiceEnum.hxNetbankPayQuery, type = ApiMessageType.Request)
public class HxNetbankPayQueryRequest extends HxRequest {

    @XStreamAlias("OrderQueryReq")
    private OrderQueryReq orderQueryReq;

}
