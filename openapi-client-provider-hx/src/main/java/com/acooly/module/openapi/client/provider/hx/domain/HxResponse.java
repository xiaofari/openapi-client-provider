/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.hx.domain;

import com.acooly.module.openapi.client.provider.hx.enums.HxServiceEnum;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.io.naming.NoNameCoder;
import com.thoughtworks.xstream.io.xml.DomDriver;

import lombok.Getter;
import lombok.Setter;

/**
 * Fuiou 响应报文基类
 *
 *
 * @author zhangpu
 */
@Getter
@Setter
public class HxResponse extends HxApiMessage {

	@XStreamOmitField
	private XStream xStream;

	/**
	 * 数据类型 xml/json
	 *
	 * @param
	 */
	public HxResponse() {
		xStream = new XStream(new DomDriver("UTF-8", new NoNameCoder()));
		// 启用Annotation
		xStream.autodetectAnnotations(true);
	}

	public Object str2Obj(String xml,Object root,String serviceCode){
		if(HxServiceEnum.hxWithdraw.code().equals(serviceCode)){
			xStream.alias("Rep", root.getClass());
		}else{
			xStream.alias("Ips", root.getClass());
		}
		return xStream.fromXML(xml,root);
	}


}
