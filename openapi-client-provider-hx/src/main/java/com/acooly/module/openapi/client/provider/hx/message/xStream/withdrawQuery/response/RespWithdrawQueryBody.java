package com.acooly.module.openapi.client.provider.hx.message.xStream.withdrawQuery.response;


import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("body")
public class RespWithdrawQueryBody {

    @XStreamAlias("IssuedTradeList")
    private IssuedTradeList issuedTradeList;

}
