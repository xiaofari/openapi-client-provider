<!-- title: 微信支付SDK组件 -->
<!-- type: gateway -->
<!-- author: jiandao -->

## 1. 提供能力

组件的集成模式为标准组件集成方式。直接通过pom文件引入依赖并配置对应的参数即可。

## 2. 使用说明

```xml
<dependency>
  <groupId>com.acooly</groupId>
  <artifactId>openapi-client-provider-weixin</artifactId>
  <version>4.2.0-SNAPSHOT</version>
</dependency>
```

* 直接采用第三方微信的SDK，本主键主要扩展异步通知处理
# 仅供测试使用,外部请自行下载 com.github.binarywang weixin-java-pay 的sdk使用
