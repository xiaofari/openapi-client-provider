package com.acooly.module.openapi.client.provider.newyl.enums;

import org.dom4j.Document;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import java.io.StringWriter;

/**
 * 银联参数定义类
 * @author SongCheng
 * 2015年9月17日下午4:15:15
 */
public class NewYl {
	public static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(NewYl.class);

	public static final String TRX_CODE_交易代码_批量 = "100001";
	public static final String TRX_CODE_交易代码_实时 = "100004";
	public static final String TRX_CODE_交易代码_结果查询 = "200001";
	public static final String TRX_CODE_交易代码_实时代付="100005";
	public static final String TRX_CODE_交易代码_批量代付="100002";
	public static final String VERSION_版本 = "05";
	public static final String DATA_TYPE_数据格式_XML = "2";
	public static final String LEVEL_处理级别_0 = "0";//0-9  0实时处理  默认5
	public static final String LEVEL_处理级别_5 = "5";//0-9  0实时处理  5 批量
	public static final String BUSINESS_CODE_业务代码_贷款 = "14900";//其他费用
	public static final String BUSINESS_CODE_业务代码_贷款_DF = "04900";//其他费用
//	其他费用	00	14900
//	还贷	01	14901
//	货款	02	14902

	

	public static String formatXml_UTF8(Document doc, String encoding) {
		try {
			OutputFormat format = OutputFormat.createPrettyPrint();
			//format.setEncoding(S.ENCODING_utf8);
			format.setEncoding(encoding);
			format.setIndent(false);
			format.setNewlines(false);
			format.setNewLineAfterDeclaration(false);
			/*
			format.setLineSeparator("\n");
			 */
			StringWriter out = new StringWriter();
			XMLWriter writer = new XMLWriter(out, format);
			writer.write(doc);
			writer.close();
			return out.toString();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}//method
	
	

}//class
