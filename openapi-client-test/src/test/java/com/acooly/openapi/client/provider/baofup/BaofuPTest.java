package com.acooly.openapi.client.provider.baofup;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.baofu.BaoFuConstants;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuBillDowloadRequest;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuBillDowloadResponse;
import com.acooly.module.openapi.client.provider.baofup.BaoFuPApiService;
import com.acooly.module.openapi.client.provider.baofup.message.*;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

/**
 * @author zhike 2018/1/26 18:03
 */
@SpringBootApplication
@BootApp(sysName = "test")
public class BaofuPTest extends NoWebTestBase {

    @Autowired
    private BaoFuPApiService baoFuPApiService;

    /**
     * 测试协议支付预绑卡接口
     */
    @Test
    public void testProtocolPayApplyBindCard() {
        BaoFuProtocolPayApplyBindCardRequest request = new BaoFuProtocolPayApplyBindCardRequest();
        request.setMsgId(Ids.oid());
//        request.setUserId(Ids.oid());
        request.setBankCardNo("6222801216771015408");
        request.setRealName("宋阳");
        request.setIdCardNo("320924198610270018");
        request.setMobileNo("13601921997");
        BaoFuProtocolPayApplyBindCardResponse response = baoFuPApiService.protocolPayApplyBindCard(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试协议支付确认绑卡接口
     */
    @Test
    public void testProtocolPayConfirmBindCard() {
        BaoFuProtocolPayConfirmBindCardRequest request = new BaoFuProtocolPayConfirmBindCardRequest();
        request.setMsgId(Ids.oid());
        request.setUniqueCode("201806201308213083708");
        request.setSmsCode("536952");
        BaoFuProtocolPayConfirmBindCardResponse response = baoFuPApiService.protocolPayConfirmBindCard(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试协议支付接口
     */
    @Test
    public void testProtocolPay() {
        BaoFuProtocolPayRequest request = new BaoFuProtocolPayRequest();
        request.setMsgId(Ids.oid());
        request.setProtocolNo("1201806201310002750001883709");
        request.setAmount("1");
//        request.setUserId("o18060511264896240002");
        request.setTransId(Ids.oid());
        request.setRiskItem("{\"goodsCategory\":\"02\",\"chPayIp\":\"127.0.0.1\"}");
        BaoFuProtocolPayResponse response = baoFuPApiService.protocolPay(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试协议预支付接口
     */
    @Test
    public void testProtocolApplyPay() {
        BaoFuProtocolApplyPayRequest request = new BaoFuProtocolApplyPayRequest();
        request.setMsgId(Ids.oid());
        request.setProtocolNo("1201806051128383430001843458");
        request.setAmount("1");
        request.setUserId("o18060511264896240002");
        request.setTransId(Ids.oid());
        request.setRiskItem("{\"goodsCategory\":\"02\",\"chPayIp\":\"127.0.0.1\"}");
        BaoFuProtocolApplyPayResponse response = baoFuPApiService.protocolApplyPay(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试协议预支付接口
     */
    @Test
    public void testProtocolConfirmPay() {
        BaoFuProtocolConfirmPayRequest request = new BaoFuProtocolConfirmPayRequest();
        request.setMsgId(Ids.oid());
        request.setUniqueCode("20180605113832013031108956357243");
        request.setSmsCode("536952");
        BaoFuProtocolConfirmPayResponse response = baoFuPApiService.protocolConfirmPay(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试协议支付绑定结果查询接口
     */
    @Test
    public void testProtocolPayQueryBindCard() {
        BaoFuProtocolPayQueryBindCardRequest request = new BaoFuProtocolPayQueryBindCardRequest();
        request.setMsgId(Ids.oid());
        request.setUserId("o18041611562775440002");
        BaoFuProtocolPayQueryBindCardResponse response = baoFuPApiService.protocolPayQueryBindCard(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试协议支付查询接口
     */
    @Test
    public void testProtocolPayQuery() {
        BaoFuProtocolPayQueryRequest request = new BaoFuProtocolPayQueryRequest();
        request.setMsgId(Ids.oid());
        request.setOrigTransId("o18062013123123640002");
        request.setOrigTransDate(Dates.format(new Date()));
        BaoFuProtocolPayQueryResponse response = baoFuPApiService.protocolPayQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试解绑接口
     */
    @Test
    public void testProtocolPayUnbindCard() {
        BaoFuProtocolPayUnBindCardRequest request = new BaoFuProtocolPayUnBindCardRequest();
        request.setMsgId(Ids.oid());
        request.setUserId("o18060510495380760002");
        request.setProtocolNo("1201806051052161510001843033");
        BaoFuProtocolPayUnBindCardResponse response = baoFuPApiService.protocolPayUnBindCard(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试对账文件下载
     */
    @Test
    public void testBillDownload() {
        BaoFuPBillDowloadRequest request = new BaoFuPBillDowloadRequest();
        request.setGatewayUrl(BaoFuConstants.BAOFU_DOWNLOAD_BILL_URL);
        request.setFileType("fi");
        request.setClientIp("113.250.252.77");
        request.setSettleDate("2018-01-20");
//        request.setFilePath("D:/var");
        request.setFileName("aa");
        request.setFileSuffix("zip");
        BaoFuPBillDowloadResponse response = baoFuPApiService.billDownload(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

}
