package com.acooly.openapi.client.provider.shengpay;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.jyt.JytApiService;
import com.acooly.module.openapi.client.provider.jyt.message.*;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytDeductApplyRequestBody;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytDeductPayRequestBody;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytRetrievesSmsCodeRequestBody;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytTradeOrderQueryRequestBody;
import com.acooly.module.openapi.client.provider.shengpay.ShengpayApiService;
import com.acooly.module.openapi.client.provider.shengpay.message.*;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@BootApp(sysName = "shengpayTest")
public class ShengpayTest extends NoWebTestBase {
    @Autowired
    private ShengpayApiService shengpayApiService;

    /**
     * 测试签约预校验接口
     */
    @Test
    public void precheckForSign() {
        ShengpayPrecheckForSignRequest request = new ShengpayPrecheckForSignRequest();
        request.setBankCardNo("6217711201032972");
        request.setBankCode("CITIC");
        request.setMobileNo("18696724229");
        request.setRealName("韦崇凯");
        request.setOutMemberId(Ids.oid());
        request.setRequestNo(Ids.oid());
        request.setUserIp("127.0.0.1");
        request.setIdNo("500221193210192313");
        ShengpayPrecheckForSignResponse response = shengpayApiService.precheckForSign(request);
        System.out.println("创建支付订单接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试签约确认接口
     */
    @Test
    public void signConfirm() {
        ShengpaySignConfirmRequest request = new ShengpaySignConfirmRequest();
        request.setRequestNo("o18052214010424360002");
        request.setValidateCode("740772");
        ShengpaySignConfirmResponse response = shengpayApiService.signConfirm(request);
        System.out.println("签约确认接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试创建支付订单接口
     */
    @Test
    public void createPaymentOrder() {
        ShengpayCreatePaymentOrderRequest request = new ShengpayCreatePaymentOrderRequest();
        request.setAmount("0.01");
        request.setMerchantOrderNo(Ids.oid());
        request.setProductName("测试");
        request.setUserIp("127.0.0.1");
        ShengpayCreatePaymentOrderResponse response = shengpayApiService.createPaymentOrder(request);
        System.out.println("创建支付订单接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试支付预校验接口
     */
    @Test
    public void precheckForPayment() {
        ShengpayPrecheckForPaymentRequest request = new ShengpayPrecheckForPaymentRequest();
        request.setSessionToken("20180522140212306224");
        request.setAgreementNo("19236290");
        request.setIsResendValidateCode("false");
        request.setOutMemberId("o18052214010403360001");
        request.setBankCode("CITIC");
        request.setBankCardNo("6217711201082372");
        request.setRealName("韦崇凯");
        request.setIdNo("500221193210192313");
        request.setMobileNo("18696725229");
        request.setRiskExtItems(
            "{\"outMemberId\":\"o18052214010403360001\","
                + "\"outMemberRegistTime\":\"20110707112233\","
                + "\"outMemberRegistIP\":\"127.0.0.1\","
                + "\"outMemberVerifyStatus\":\"0\","
                + "\"outMemberName\":\"韦崇凯\","
                + "\"outMemberMobile\":\"18696725229\""
            + "}");
        request.setUserIp("127.0.0.1");
        ShengpayPrecheckForPaymentResponse response = shengpayApiService.precheckForPayment(request);
        System.out.println("支付预校验接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试支付确认接口
     */
    @Test
    public void paymentConfirm() {
        ShengpayPaymentConfirmRequest request = new ShengpayPaymentConfirmRequest();
        request.setSessionToken("20180522140212306224");
        request.setValidateCode("1223123");
        request.setUserIp("127.0.0.1");
        ShengpayPaymentConfirmResponse response = shengpayApiService.paymentConfirm(request);
        System.out.println("支付确认接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试协议查询接口
     */
    @Test
    public void queryAgreement() {
        ShengpayQueryAgreementRequest request = new ShengpayQueryAgreementRequest();
        request.setAgreementNo("0.01");
        request.setBankCode("1222323");
        request.setOutMemberId("127.0.0.1");
        ShengpayQueryAgreementResponse response = shengpayApiService.queryAgreement(request);
        System.out.println("创建支付订单接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试协解约接口
     */
    @Test
    public void unsign() {
        ShengpayUnsignRequest request = new ShengpayUnsignRequest();
        request.setAgreementNo("19394926");
        request.setPrincipalId("o18052212205150760001");
        ShengpayUnsignResponse response = shengpayApiService.unsign(request);
        System.out.println("解约接口响应报文："+ JSON.toJSONString(response));
    }


    /**
     * 测试单笔订单查询接口
     */
    @Test
    public void singleOrderQuery() {
        ShengpaySingleOrderQueryRequest request = new ShengpaySingleOrderQueryRequest();
        request.setOrderNo("o18052214032493760001");
        ShengpaySingleOrderQueryResponse response = shengpayApiService.singleOrderQuery(request);
        System.out.println("单笔订单查询接口响应报文："+ JSON.toJSONString(response));
    }
}
