package com.acooly.openapi.client.provider.wsbank.bkmerchanttradePayCancel;

import com.acooly.core.common.BootApp;
import com.acooly.module.openapi.client.provider.wsbank.WsbankApiService;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankBkmerchanttradePayCancelRequest;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankBkmerchanttradePayCancelResponse;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBkmerchanttradePayCancelRequestBody;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBkmerchanttradePayCancelRequestInfo;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@BootApp(sysName = "wsbankBkmerchanttradePayCancelTest")
public class WsbankBkmerchanttradePayCancelTest extends NoWebTestBase {
    @Autowired
    private WsbankApiService wsbankApiService;
    
    /**
     * 测试订单撤销接口
     */
    @Test
    public void bkmerchanttradePayCancel() {
    	WsbankBkmerchanttradePayCancelRequestBody requestBody = new WsbankBkmerchanttradePayCancelRequestBody();
    	requestBody.setMerchantId("226801000000103460124");
    	requestBody.setOutTradeNo("o18060113354409960003");
        WsbankBkmerchanttradePayCancelRequestInfo requestInfo = new WsbankBkmerchanttradePayCancelRequestInfo();
        requestInfo.setRequestBody(requestBody);
        WsbankBkmerchanttradePayCancelRequest request = new WsbankBkmerchanttradePayCancelRequest();
        request.setRequestInfo(requestInfo);
        WsbankBkmerchanttradePayCancelResponse response = wsbankApiService.bkmerchanttradePayCancel(request);
        System.out.println("测试订单撤销接口："+ JSON.toJSONString(response));
    }
}
