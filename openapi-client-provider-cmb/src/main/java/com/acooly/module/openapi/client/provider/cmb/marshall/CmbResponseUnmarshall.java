/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.cmb.marshall;

import com.google.common.collect.Maps;

import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.cmb.domain.CmbResponse;
import com.acooly.module.openapi.client.provider.cmb.partner.CmbPartnerIdLoadManager;
import com.acooly.module.safety.Safes;
import com.acooly.module.safety.key.KeyLoadManager;
import com.acooly.module.safety.signature.SignTypeEnum;
import com.acooly.module.safety.support.KeyPair;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;

import static com.acooly.module.openapi.client.provider.cmb.utils.MapHelper.mapToBean;

/**
 * @author zhangpu
 */
@Slf4j
@Service
public class CmbResponseUnmarshall extends CmbMarshallSupport implements ApiUnmarshal<CmbResponse, String> {

    private static final Logger logger = LoggerFactory.getLogger(CmbResponseUnmarshall.class);

    private KeyPair keyPair;

    @Resource(name = "weBankMessageFactory")
    private MessageFactory messageFactory;

    @Resource(name = "weBankPartnerIdLoadManager")
    private CmbPartnerIdLoadManager partnerIdLoadManager;

    @Autowired
    private KeyLoadManager keyStoreLoadManager;


    @SuppressWarnings("unchecked")
    @Override
    public CmbResponse unmarshal(String message, String serviceName) {
        try {
            logger.info("响应报文:{}", message);

            String[] tempArray = message.split ("&signature=");
            String plain = tempArray[0];
            String sign = tempArray[1];
            logger.info ("代签名串：{}", plain);
            logger.info ("签名字符串{}", sign);

            //签名处理
            Safes.getSigner(SignTypeEnum.Rsa.name ()).verify(plain, getKeyPair(), sign);

            CmbResponse weBankResponse = (CmbResponse) messageFactory.getResponse (serviceName);
            mapToBean(parseBody(plain),weBankResponse);//解析返回的报文
            weBankResponse.setSign(sign);
            weBankResponse.setBizType(serviceName);
            weBankResponse.setPartner_id(getProperties ().getPartnerId());

            return weBankResponse;

        } catch (Exception e) {
            throw new ApiClientException("解析响应报文错误:" + e.getMessage());
        }
    }


    public static Map<String, Object> parseBody (String body) {
        if (StringUtils.isBlank (body)) {
            return null;
        }
        Map<String, Object> resultMap = Maps.newHashMap ();
        // body  merId=xx&orderId=xx&bizType=xx&respCode=xx&respMsg=xx
        String[] strs = body.split ("&");
        for (String s : strs) {
            String[] ms = s.split ("=");
            if (ms.length < 2){
                resultMap.put (ms[0],"");
            }else{
                resultMap.put (ms[0], ms[1]);
            }
        }
        return resultMap;
    }
}
