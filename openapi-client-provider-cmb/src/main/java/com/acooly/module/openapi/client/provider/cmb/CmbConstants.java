/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.cmb;

/**
 * @author
 */
public class CmbConstants {
    /**
     * 签名实现KEY和秘钥
     */
    public static final String SIGN = "signature";
    public static final String SIGN_TYPE = "RSA";
    public static final String SIGNER_KEY = "cmb";
}
