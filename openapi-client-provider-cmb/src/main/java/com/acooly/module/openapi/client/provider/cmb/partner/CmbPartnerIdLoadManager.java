/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-11-15 13:21 创建
 */
package com.acooly.module.openapi.client.provider.cmb.partner;

/**
 * 标记KeyStoreInfo的load 标记接口
 *
 * @author zhangpu 2017-11-15 13:21
 */
public interface CmbPartnerIdLoadManager {

    /**
     * 获取当前订单对应的商户id
     *
     * @param orderNo
     * @return
     */
    String load(String orderNo);
}
