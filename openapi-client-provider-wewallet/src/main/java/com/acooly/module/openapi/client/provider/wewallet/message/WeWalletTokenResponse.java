package com.acooly.module.openapi.client.provider.wewallet.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletApiMsgInfo;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletResponse;
import com.acooly.module.openapi.client.provider.wewallet.enums.WeWalletServiceEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@WeWalletApiMsgInfo(service = WeWalletServiceEnum.WEWALLET_ACCESS_TOKEN, type = ApiMessageType.Response)
public class WeWalletTokenResponse extends WeWalletResponse {

    /**
     * 交易时间
     */
    String transactionTime;

    /**
     *  access_token 失效的绝对时间戳，单位毫秒
     */
    String expireTime;

    /**
     *  access_token 的最大生存时间，单位秒
     */
    String expireIn;


    /**
     * 访问令牌
     */
    String accessToken;

}
