package com.acooly.module.openapi.client.provider.cj.message;


import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.cj.message.dto.CjBatchRetInfo;
import com.acooly.module.openapi.client.provider.cj.domain.CjApiMsgInfo;
import com.acooly.module.openapi.client.provider.cj.domain.CjResponse;
import com.acooly.module.openapi.client.provider.cj.enums.CjServiceEnum;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CjApiMsgInfo(service = CjServiceEnum.CJ_BATCH_DEDUCT, type = ApiMessageType.Response)
public class CjBatchDeductResponse extends CjResponse {

    /**
     *同步成功时返回实际金额
     */
    private Money amountLn;

    /**
     * 订单效果结果集合
     */
    private List<CjBatchRetInfo> batchRetDetils;

}
