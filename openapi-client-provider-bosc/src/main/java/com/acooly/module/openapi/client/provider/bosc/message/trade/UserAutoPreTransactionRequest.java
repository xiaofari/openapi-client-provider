package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscBizTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 授权预处理  支持交易类型:TENDER 投标,REPAYMENT 还款, CREDIT_ASSIGNMENT 债权认购, COMPENSATOR 代偿
 *
 *
 */

@ApiMsgInfo(service = BoscServiceNameEnum.USER_AUTO_PRE_TRANSACTION)
public class UserAutoPreTransactionRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotBlank
	@Size(max = 50)
	private String requestNo;
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 关联充值请求流水号（原充值成功请求流水号）
	 */
	@Size(max = 50)
	private String originalRechargeNo;
	/**
	 * 见【预处理业务类型】,若传入关联请求流水号，则业务类型需要相对应
	 */
	@NotNull
	private BoscBizTypeEnum bizType;
	/**
	 * 冻结金额
	 */
	@MoneyConstraint(min = 1)
	private Money amount;
	/**
	 * 预备使用的红包金额，只记录不冻结，仅限投标业务类型
	 */
	@MoneyConstraint(nullable = true)
	private Money preMarketingAmount;
	/**
	 * 备注
	 */
	@Size(max = 50)
	private String remark;
	/**
	 * 标的号, 若传入关联充值请求流水号，则标的号固定为充值请求传入的标的号
	 */
	@NotEmpty
	@Size(max = 50)
	private String projectNo;
	/**
	 * 购买债转份额，业务类型为债权认购时，需要传此参数
	 */
	@MoneyConstraint(min = 1,groups = CreditAssignGroup.class)
	private Money share;
	/**
	 * 债权出让请求流水号，只有债权认购业务需填此参数
	 */
	@NotBlank(groups = CreditAssignGroup.class)
	@Size(max = 50)
	private String creditsaleRequestNo;
	
	public UserAutoPreTransactionRequest () {
		setService (BoscServiceNameEnum.USER_AUTO_PRE_TRANSACTION.code ());
	}
	
	public UserAutoPreTransactionRequest (String requestNo, String platformUserNo,
	                                      BoscBizTypeEnum bizType, Money amount, String projectNo) {
		this();
		this.requestNo = requestNo;
		this.platformUserNo = platformUserNo;
		this.bizType = bizType;
		this.amount = amount;
		this.projectNo = projectNo;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getOriginalRechargeNo () {
		return originalRechargeNo;
	}
	
	public void setOriginalRechargeNo (String originalRechargeNo) {
		this.originalRechargeNo = originalRechargeNo;
	}
	
	public BoscBizTypeEnum getBizType () {
		return bizType;
	}
	
	public void setBizType (BoscBizTypeEnum bizType) {
		this.bizType = bizType;
	}
	
	public Money getAmount () {
		return amount;
	}
	
	public void setAmount (Money amount) {
		this.amount = amount;
	}
	
	public Money getPreMarketingAmount () {
		return preMarketingAmount;
	}
	
	public void setPreMarketingAmount (Money preMarketingAmount) {
		this.preMarketingAmount = preMarketingAmount;
	}
	
	public String getRemark () {
		return remark;
	}
	
	public void setRemark (String remark) {
		this.remark = remark;
	}
	
	public String getProjectNo () {
		return projectNo;
	}
	
	public void setProjectNo (String projectNo) {
		this.projectNo = projectNo;
	}
	
	public Money getShare () {
		return share;
	}
	
	public void setShare (Money share) {
		this.share = share;
	}
	
	public String getCreditsaleRequestNo () {
		return creditsaleRequestNo;
	}
	
	public void setCreditsaleRequestNo (String creditsaleRequestNo) {
		this.creditsaleRequestNo = creditsaleRequestNo;
	}
	
	public interface CreditAssignGroup{}
}