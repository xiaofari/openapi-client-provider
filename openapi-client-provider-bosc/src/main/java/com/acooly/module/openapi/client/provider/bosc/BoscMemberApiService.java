package com.acooly.module.openapi.client.provider.bosc;

import com.acooly.module.openapi.client.provider.bosc.message.member.ActivateStockedUserRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.EnterpriseBindBankcardRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.EnterpriseRegisterRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.ModifyMobileExpandRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.PersonalBindBankcardExpandRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.PersonalRegisterRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.QueryUserInfomationRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.QueryUserInfomationResponse;
import com.acooly.module.openapi.client.provider.bosc.message.member.ResetPasswordRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.UnbindBankcardRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.UnbindBankcardResponse;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by liubin@prosysoft.com on 2017/10/10.
 */
@Component
public class BoscMemberApiService {
	
	@Resource(name = "boscApiServiceClient")
	private BoscApiServiceClient boscApiServiceClient;
	
	/**
	 * 个人用户注册(跳转)
	 *
	 * @param request
	 *
	 * @return
	 */
	public String personalRegisterRedirect (PersonalRegisterRequest request) {
		return boscApiServiceClient.redirectGet (request);
	}
	
	
	/**
	 * 企业用户注册(跳转)
	 *
	 * @param request
	 *
	 * @return
	 */
	public String enterpriseRegisterRedirect (EnterpriseRegisterRequest request) {
		return boscApiServiceClient.redirectGet (request);
	}
	
	/**
	 * 会员信息查询
	 *
	 * @param request
	 *
	 * @return
	 */
	public QueryUserInfomationResponse queryUserInfomation (QueryUserInfomationRequest request) {
		return (QueryUserInfomationResponse) boscApiServiceClient.execute (request);
	}
	
	/**
	 * 解绑银行卡重定向
	 * @param request
	 * @return
	 */
	public String unbindBankcard(UnbindBankcardRequest request){
		return boscApiServiceClient.redirectGet (request);
	}
	
	/**
	 * 个人会员绑卡跳转地址
	 * @return
	 */
	public String personalBindBankcardRedirect(PersonalBindBankcardExpandRequest request){
		return boscApiServiceClient.redirectGet (request);
	}
	
	/**
	 * 企业会员绑卡跳转地址
	 * @param request
	 * @return
	 */
	public String enterpriseBindBankcardRedirect(EnterpriseBindBankcardRequest request){
		return boscApiServiceClient.redirectGet (request);
	}
	
	/**
	 * 变更预留手机号跳转地址
	 * @param request
	 * @return
	 */
	public String modifyMobileRedirect(ModifyMobileExpandRequest request){
		return boscApiServiceClient.redirectGet (request);
	}
	
	/**
	 * 修改支付密码跳转地址
	 * @param request
	 * @return
	 */
	public String resetPasswordRedirect(ResetPasswordRequest request){
		return boscApiServiceClient.redirectGet (request);
	}
	
	/**
	 * 会员激活
	 * @param request
	 * @return
	 */
	public String activateStockedUserRedirect(ActivateStockedUserRequest request){
		return boscApiServiceClient.redirectGet (request);
	}
}
