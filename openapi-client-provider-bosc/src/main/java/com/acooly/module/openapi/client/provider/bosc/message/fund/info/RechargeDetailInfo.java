/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.bosc.message.fund.info;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscBankcodeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscExpectPayCompanyEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscRechargeStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscRechargeWayEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by liubin@prosysoft.com on 2017/10/16.
 */
public class RechargeDetailInfo implements Serializable {
	
	/**
	 * 充值金额
	 */
	@MoneyConstraint
	private Money amount;
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max=50)
	private String platformUserNo;
	/**
	 * 交易发起时间
	 */
	@NotNull
	private Date createTime;
	/**
	 * 交易完成时间
	 */
	@NotNull
	private Date transactionTime;
	/**
	 * SUCCESS  表示支付成功，FAIL 表示支付失败，ERROR 表示支付错误，PENDDING 表示支付中
	 */
	@NotNull
	private BoscRechargeStatusEnum status;
	/**
	 * 见【支付方式】
	 */
	@NotNull
	private BoscRechargeWayEnum rechargeWay;
	/**
	 * 见【银行编码】
	 */
	private BoscBankcodeEnum bankcode;
	/**
	 * 见【支付公司】
	 */
	@NotNull
	private BoscExpectPayCompanyEnum payCompany;
	/**
	 * 支付公司订单号
	 */
	@Size(max=60)
	private String payCompanyRequestNo;
	/**
	 * 【存管错误码】
	 */
	@Size(max=50)
	private String errorCode;
	/**
	 * 【存管错误描述】
	 */
	@Size(max=200)
	private String errorMessage;
	/**
	 * 【支付通道错误码】
	 */
	@Size(max=50)
	private String channelErrorCode;
	/**
	 * 【支付ß通道返回错误消息】
	 */
	@Size(max=200)
	private String channelErrorMessage;
	
	public Money getAmount () {
		return amount;
	}
	
	public void setAmount (Money amount) {
		this.amount = amount;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public Date getCreateTime () {
		return createTime;
	}
	
	public void setCreateTime (Date createTime) {
		this.createTime = createTime;
	}
	
	public Date getTransactionTime () {
		return transactionTime;
	}
	
	public void setTransactionTime (Date transactionTime) {
		this.transactionTime = transactionTime;
	}
	
	public BoscRechargeStatusEnum getStatus () {
		return status;
	}
	
	public void setStatus (BoscRechargeStatusEnum status) {
		this.status = status;
	}
	
	public BoscRechargeWayEnum getRechargeWay () {
		return rechargeWay;
	}
	
	public void setRechargeWay (BoscRechargeWayEnum rechargeWay) {
		this.rechargeWay = rechargeWay;
	}
	
	public BoscBankcodeEnum getBankcode () {
		return bankcode;
	}
	
	public void setBankcode (BoscBankcodeEnum bankcode) {
		this.bankcode = bankcode;
	}
	
	public BoscExpectPayCompanyEnum getPayCompany () {
		return payCompany;
	}
	
	public void setPayCompany (BoscExpectPayCompanyEnum payCompany) {
		this.payCompany = payCompany;
	}
	
	public String getPayCompanyRequestNo () {
		return payCompanyRequestNo;
	}
	
	public void setPayCompanyRequestNo (String payCompanyRequestNo) {
		this.payCompanyRequestNo = payCompanyRequestNo;
	}
	
	public String getErrorCode () {
		return errorCode;
	}
	
	public void setErrorCode (String errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getErrorMessage () {
		return errorMessage;
	}
	
	public void setErrorMessage (String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getChannelErrorCode () {
		return channelErrorCode;
	}
	
	public void setChannelErrorCode (String channelErrorCode) {
		this.channelErrorCode = channelErrorCode;
	}
	
	public String getChannelErrorMessage () {
		return channelErrorMessage;
	}
	
	public void setChannelErrorMessage (String channelErrorMessage) {
		this.channelErrorMessage = channelErrorMessage;
	}
}
