package com.acooly.module.openapi.client.provider.yl.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.yl.domain.YlRequest;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class YlRedirectPostMarshall extends YlMarshallSupport implements ApiMarshal<PostRedirect, YlRequest> {

    @Override
    public PostRedirect marshal(YlRequest source) {

        return null;
    }

}
