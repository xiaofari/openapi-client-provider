/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月1日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.annotation;


import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zhike
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface SinaGateWay {

	public SinapayGateWayType value() default SinapayGateWayType.MEMBER;

}
