package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/7/10 14:38
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_CTRL_RESULT, type = ApiMessageType.Response)
public class QueryCtrlResultResponse extends SinapayResponse {

    /**
     * 冻结解冻订单号
     * 商户网站冻结或解冻订单号，商户内部保证唯一
     */
    @Size(max = 32)
    @NotEmpty
    @ApiItem("out_ctrl_no")
    private String outCtrlNo;

    /**
     * 订单状态
     * 冻结解冻订单状态，详见附录“支付状态”
     */
    @Size(max = 16)
    @ApiItem("ctrl_status")
    private String ctrlStatus;

    /**
     * 冻结解冻失败原因
     */
    @ApiItem("error_msg")
    private String errorDesc;

}
