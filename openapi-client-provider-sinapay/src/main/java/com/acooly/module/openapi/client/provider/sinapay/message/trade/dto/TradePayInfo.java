/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月13日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade.dto;

import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ApiDto;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ItemOrder;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayAccountType;
import com.acooly.module.openapi.client.provider.sinapay.message.Dtoable;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * 代付项信息
 * 
 * @author zhike
 */
@Getter
@Setter
@ApiDto(colSeparator = '~', rowSeparator = '$')
public class TradePayInfo implements Dtoable {

	/**
	 * 交易订单号
	 *
	 * 商户网站交易订单号，商户内部保证唯一
	 */
	@NotEmpty
	@Size(max = 32)
	@ItemOrder(0)
	private String outTradeNo = Ids.oid();

	/**
	 * 收款用户ID
	 *
	 * 商户系统用户ID(字母或数字)
	 */
	@NotEmpty
	@Size(max = 32)
	@ItemOrder(1)
	private String payeeId;

	/**
	 * 标识类型
	 *
	 * ID的类型，参考“标志类型”
	 */
	@NotEmpty
	@Size(max = 16)
	@ItemOrder(2)
	private String identityType = "UID";

	/**
	 * 账户类型
	 *
	 * 账户类型（基本户、保证金户,存钱罐、银行账户）。默认基本户，见附录
	 */
	@Size(max = 16)
	@ItemOrder(3)
	private String accountType = SinapayAccountType.SAVING_POT.code();

	@MoneyConstraint
	@ItemOrder(4)
	private Money amount;

	/**
	 * 分账信息列表
	 *
	 * 收款信息列表，参见收款信息，参数间用“^”分隔，各条目之间用“|”分隔，备注信息不要包含特殊分隔符
	 */
	@ItemOrder(5)
	private List<TradePayItem> splitList = Lists.newArrayList();

	/** 摘要 */
	@NotEmpty
	@Size(max = 64)
	@ItemOrder(6)
	private String memo;

	@Size(max = 200)
	@ItemOrder(7)
	private String extendParam;

	/**
	 * 用户承担的手续费金额 预留属性，暂不支持
	 */
	@MoneyConstraint(nullable = true)
	@ItemOrder(8)
	private Money userFee;

	/** 标ID */
	@Size(max = 64)
	@ItemOrder(9)
	private String tenderId;

	/** 在权变动明细列表   ps.此字段目前只针对接入恒丰存管的商户，非恒丰商户此字段可空 */
	@Size(max = 300)
	@ItemOrder(10)
	private String borrowChangeDetail;


	/** 交易关联号 */
	@Size(max = 32)
	@ItemOrder(11)
	@NotEmpty
	private String tradeRelatedNo;

	public TradePayInfo() {
		super();
	}

	/**
	 * @param payeeId
	 * @param amount
	 * @param memo
	 */
	public TradePayInfo(String payeeId, Money amount, String memo) {
		super();
		this.payeeId = payeeId;
		this.amount = amount;
		this.memo = memo;
	}

	public TradePayInfo(String outTradeNo, String payeeId, Money amount, String memo) {
		super();
		this.outTradeNo = outTradeNo;
		this.payeeId = payeeId;
		this.amount = amount;
		this.memo = memo;
	}
}
