package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/7/10 15:29
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_TRADE_RELATED_NO, type = ApiMessageType.Request)
public class QueryTradeRelatedNoRequest extends SinapayRequest {

    /**
     *交易关联号
     *商户交易关联号，用于代收付交易关联
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "trade_related_no")
    private String tradeRelatedNo;
}
