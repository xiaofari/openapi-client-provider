/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.alipay.domain;

import com.acooly.module.openapi.client.provider.alipay.support.AlipayAlias;

import lombok.Getter;
import lombok.Setter;

/** @author zhangpu */
@Getter
@Setter
public class AlipayRequest extends AlipayApiMessage {

	/**
	 * 商户订单号
	 */
	@AlipayAlias(value = "out_trade_no")
	private String outTradeNo;
}
