package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankNotify;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankOrdershareNotifyInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/24 14:48
 */
@Getter
@Setter
@XStreamAlias("document")
@WsbankApiMsgInfo(service = WsbankServiceEnum.ORDERSHARE_NOTIFY,type = ApiMessageType.Notify)
public class WsbankOrdershareNotify extends WsbankNotify {

    /**
     * 分账通知
     */
    @XStreamAlias("request")
    private WsbankOrdershareNotifyInfo requestInfo;
}
