package com.acooly.module.openapi.client.provider.wsbank.message.base;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @author weili 2018/5/22 18:14
 */
@Getter
@Setter
public class SubMerchantDetail implements Serializable {

	/**
	 * 商户简称。
	 */
	private String alias;

	/**
	 * 联系人手机号。为商户常用联系人联系手机号。
	 */
	private String contactMobile;

	/**
	 * 联系人姓名。为商户常用联系人姓名。
	 */
	private String contactName;

	/**
	 * 省份
	 */
	private String province;

	/**
	 * 城市
	 */
	private String city;

	/**
	 * 区
	 */
	private String district;

	/**
	 * 地址
	 */
	private String address;
	
	 @Override
	public String toString() {
	        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
