package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.core.utils.Dates;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankAlias;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankRequest;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.File;
import java.util.Date;

/**
 * @author zhike 2018/5/23 14:31
 */
@Getter
@Setter
@WsbankApiMsgInfo(service = WsbankServiceEnum.UPLOAD_IMG,type = ApiMessageType.Request)
public class WsbankUploadImgRequest extends WsbankRequest {

    /**
     * 合作方机构号（网商银行分配）
     */
    @NotBlank
    @WsbankAlias("IsvOrgId")
    private String isvOrgId;

    /** 图片类型。可选值： 01 身份证正面 02 身份证反面 03 营业执照 04组织机构代码证 05 开户许可证 06 门头照 07 其他 */
    @NotBlank
    @WsbankAlias("PhotoType")
    private String photoType;

    /**
     * 外部交易号
     */
    @NotBlank
    @Size(max = 64)
    @WsbankAlias("OutTradeNo")
    private String outTradeNo;

    /**
     * 图片流
     * 图片不能超过10M
     */
    @NotNull
    @WsbankAlias(value = "Picture",sign = false)
    private File picture;

    /**
     * 接口名称
     */
    @NotBlank
    @WsbankAlias("Function")
    private String function = WsbankServiceEnum.UPLOAD_IMG.getKey();

    /**
     * 接口版本
     */
    @NotBlank
    @WsbankAlias("Version")
    private String version = "1.0.0";

    /**
     *应用ID。由浙江网商银行统一分配,用于识别合作伙伴应用系统，即对端系统编号。注意此字段的大小写要求
     */
    @NotBlank
    @WsbankAlias("AppId")
    private String appId;

    /**
     *发起时间。格式：yyyyMMddHHmmss，请求发起时间
     */
    @NotBlank
    @WsbankAlias("ReqTime")
    private String reqTime = Dates.format(new Date(),"yyyyMMddHHmmss");

    /**
     * 签名。对部分字段做签名，参考下面的签名要求
     */
    @NotBlank
    @WsbankAlias(value = "Signature",sign = false)
    private String signature;
}
