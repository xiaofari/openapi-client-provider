/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.fbank.domain;

import com.acooly.module.openapi.client.provider.fbank.support.FbankAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * Fuiou 响应报文基类
 *
 * @author zhike
 */
@Getter
@Setter
public class FbankResponse extends FbankApiMessage {

  /**
   * 应答码
   * 10000代表预请求成功
   * com.acooly.module.openapi.client.provider.fbank.enums.FbankRespCodeEnum
   */
  @FbankAlias("code")
  private String code;

  /**
   * 应答信息
   */
  @FbankAlias("msg")
  private String msg;
}
