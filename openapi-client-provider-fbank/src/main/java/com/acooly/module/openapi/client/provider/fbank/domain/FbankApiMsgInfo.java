/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-26 02:01 创建
 */
package com.acooly.module.openapi.client.provider.fbank.domain;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** @author zhike 2017-09-26 02:01 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface FbankApiMsgInfo {
  /**
   * 报文类型
   *
   * @return
   */
  ApiMessageType type() default ApiMessageType.Request;

  /**
   * 服务名称
   *
   * @return
   */
  FbankServiceEnum service() default FbankServiceEnum.DEFAULT;

  /**
   * 服务类型
   * @return
   */
  FbankServiceTypeEnum serviceType() default  FbankServiceTypeEnum.mer;

  /**
   * 接口是否需要验签
   * @return
   */
  boolean isVerifySign() default true;


}
