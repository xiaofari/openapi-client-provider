使用帮助
====

# 集成

组件的集成模式为标准组件集成方式。直接通过pom文件引入依赖并配置对应的参数即可。

## 依赖

```xml
<dependency>
  <groupId>com.acooly</groupId>
  <artifactId>openapi-client-provider-fbank</artifactId>
  <version>4.1.0-SNAPSHOT</version>
</dependency>
```

* 直接采用fbank的SDK，然后扩展异步通知处理
#使用方式：
###在自己工程注入FbankApiService

###此sdk提供富民的微信正扫、微信反扫、微信公众号、订单关闭、订单撤销、支付订单查询、退款订单查询

###密钥加载
* 威富通的验签签名统一使用MD5的方式
   
###示例
* 本sdk中微信公众号、支付宝主扫支付、微信主扫的异步通知地址固定值，所以调用的时候不需要传入notify_url
   * 接收异步通知的时候实现接口NotifyHandler，重新handleNotify和serviceKey方法，其中serviceKey方法需要绑定的key值在枚举FbankServiceEnum.getKey()可以获取到
   异步通知接收示例：
```java
@Slf4j
@Service
public class FbankPrepayNotifyService implements NotifyHandler {

    @Override
    public void handleNotify(ApiMessage notify) {
        FbankPrepayNotify fbankPrepayNotify = (FbankPrepayNotify) notify;
        log.info("富民银行聚合支付异步通知报文:{}", JSON.toJSONString(fbankPrepayNotify));
    }

    @Override
    public String serviceKey() {
        return FbankServiceEnum.PREPAY_API.getKey();
    }
}
```

###接口的服务码和对应的唯一表示统一放在com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum枚举里面：
* code:表示富民银行接口请求编码
* key:表示组建为每个接口定义的唯一标识
* message:表示接口名称

<font color='#DC143C'>注意：</font>调用接口的时候只需要传入响应的业务参数，公共参数如商户号，接口的版本号非特殊情况不需要手动传入，charset默认为UTF-8
     配置文件方式不用传入请求地址

###sdk提供了单独签名、验签、异步实体转化方法

###本组建支持密钥和商户号手动传入方式
* 主要解决密钥托管。由于异步通知需要验签，所以如果使用密钥托管方式异步通知需要动态根据商户号获取对应的密钥，解决方案如下代码
```java
    @Component
    public class FbankLoadKeyStoreService extends AbstractKeyLoadManager<String> implements KeySimpleLoader {
    
        @Autowired
        private BankKeyManageService bankKeyManageService;
    
        @Override
        public String doLoad(String principal) {
            BankKeyManage bankKeyManage = bankKeyManageService.getBankKeyByPartnerId(principal);
            if (bankKeyManage == null) {
                throw new BusinessException("密钥加载失败");
            } else {
                return bankKeyManage.getPartnerKey();
            }
        }
    
        @Override
        public String getProvider() {
            return FbankConstants.PROVIDER_NAME;
        }
    }
```

###非密钥托管方式配置文件示例(密钥托管只是不需要配置商户号和密钥)：
acooly.openapi.client.fbank.enable=true
acooly.openapi.client.fbank.payolGatewayUrl=http://payol.cqfmbank.com
acooly.openapi.client.fbank.merGatewayUrl=http://mer.cqfmbank.com
acooly.openapi.client.fbank.partnerId=0000000612
acooly.openapi.client.fbank.publicKey=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC0dYM3DXkVg9q+WcNjBPWaUwKoeRMrwdE4p4F6fiztv/Ys6F5AxGCbFW5UfbtbQavMp9Rrg3+8mJ5/Lp8sjf471NFe6EvbCcVwJ63Q6fA4xVyCAE7mQdfAlpCk9WKN7Qa/HqwO/OM6JDyOyycnjnNi3f3K2tK/JbWd/SHYOSMEDQIDAQAB
acooly.openapi.client.fbank.md5Key=48c51b03519621b6cca0ec3a57c456d8
acooly.openapi.client.fbank.connTimeout=10000
acooly.openapi.client.fbank.readTimeout=30000
acooly.openapi.client.fbank.domain=http://rpv2jf.natappfree.cc

###测试用例
```java
     /**
         * 聚合支付测试(主扫)
         */
        @Test
        public void testPrepay51() {
            FbankPrepayRequest request = new FbankPrepayRequest();
            request.setMchntOrderNo(Ids.oid());
            request.setPayPowerId("51");//42：公众号，50：被扫，51主扫
            request.setAmount("1");
            request.setClientIp(IPUtil.getFirstNoLoopbackIPV4Address());
            request.setSubject("测试扫码支付商品名称");
            request.setBody("测试扫码支付商品描述");
            FbankPrepayResponse response = fbankApiService.prepay(request);
            System.out.println("聚合支付接口响应报文："+ JSON.toJSONString(response));
    
        }
    
        /**
         * 聚合支付测试(被扫)
         */
        @Test
        public void testPrepay50() {
            FbankPrepayRequest request = new FbankPrepayRequest();
            request.setMchntOrderNo(Ids.oid());
            request.setPayPowerId("50");//42：公众号，50：被扫，51主扫
            request.setAmount("1");
            request.setAuthCode("135700129694582583");
            request.setTerminalId("wap00001");
            request.setClientIp(IPUtil.getFirstNoLoopbackIPV4Address());
            request.setSubject("测试被扫支付商品名称");
            request.setBody("测试被扫支付商品描述");
            FbankPrepayResponse response = fbankApiService.prepay(request);
            System.out.println("聚合支付接口响应报文："+ JSON.toJSONString(response));
    
        }
    
        /**
         * 聚合支付测试(公众号)
         */
        @Test
        public void testPrepay42() {
            FbankPrepayRequest request = new FbankPrepayRequest();
            request.setMchntOrderNo(Ids.oid());
            request.setPayPowerId("42");//42：公众号，50：被扫，51主扫
            request.setAmount("1");
            request.setSubOpenId("oJ6bEwcKMRd9MLRngLh01g7b9DsI");
            request.setSubAppId("wx16b1acc380cad70e");
            request.setClientIp(IPUtil.getFirstNoLoopbackIPV4Address());
            request.setSubject("测试被扫支付商品名称");
            request.setBody("测试被扫支付商品描述");
            FbankPrepayResponse response = fbankApiService.prepay(request);
            System.out.println("聚合支付接口响应报文："+ JSON.toJSONString(response));
    
        }
    
        /**
         * 交易退款
         */
        @Test
        public void testTradeRefund() {
            FbankTradeRefundRequest request = new FbankTradeRefundRequest();
            request.setMchntOrderNo("o18091115363772680001");
            request.setReason("任性想退");
            request.setRefMchntOrderNO(Ids.oid());
            request.setRefundFee("1");
            FbankTradeRefundResponse response = fbankApiService.tradeRefund(request);
            System.out.println("交易退款接口响应报文："+ JSON.toJSONString(response));
        }
    
        /**
         * 交易退款查询
         */
        @Test
        public void testTradeRefundQuery() {
            FbankTradeRefundQueryRequest request = new FbankTradeRefundQueryRequest();
            request.setRefMchntOrderNo("o18091115381171880001");
    //        request.setMchntOrderNo("o18091115363772680001");
            request.setOrderNo("15366513976982762308");
            FbankTradeRefundQueryResponse response = fbankApiService.tradeRefundQuery(request);
            System.out.println("交易退款查询接口响应报文："+ JSON.toJSONString(response));
        }
    
        /**
         * 交易撤销
         */
        @Test
        public void testTradeCancel() {
            FbankTradeCancelRequest request = new FbankTradeCancelRequest();
            request.setMchntOrderNo("o18091015414352600001");
            FbankTradeCancelResponse response = fbankApiService.tradeCancel(request);
            System.out.println("交易撤销接口响应报文："+ JSON.toJSONString(response));
        }
    
        /**
         * 交易关闭
         */
        @Test
        public void testTradeClose() {
            FbankTradeCloseRequest request = new FbankTradeCloseRequest();
            request.setMchntOrderNo("o18091010202539680001");
            FbankTradeCloseResponse response = fbankApiService.tradeClose(request);
            System.out.println("交易关闭接口响应报文："+ JSON.toJSONString(response));
        }
    
    
        /**
         * 查询用户信息
         */
        @Test
        public void testTradeQuery() {
            FbankTransQueryRequest request = new FbankTransQueryRequest();
            request.setMchntOrderNo("o18091015473731280001");
            FbankTransQueryResponse response = fbankApiService.transQuery(request);
            System.out.println("订单查询接口响应报文："+ JSON.toJSONString(response));
        }
```

