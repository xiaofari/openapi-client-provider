/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.fuyou.domain;

import com.acooly.module.openapi.client.provider.fuyou.support.FuyouAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Fuiou 响应报文基类
 * 
 * 
 * @author zhangpu
 */
public class FuyouResponse extends FuyouApiMessage {

	@XStreamAlias("resp_code")
	@FuyouAlias("resp_code")
	private String respCode;

	private String message;

	/**
	 * 响应代码
	 * 0000 成功
	 */
	@XStreamAlias("RESPONSECODE")
	private String responseCode;

	/**
	 * 响应中文描述
	 */
	@XStreamAlias("RESPONSEMSG")
	private String responseMsg;

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
}
