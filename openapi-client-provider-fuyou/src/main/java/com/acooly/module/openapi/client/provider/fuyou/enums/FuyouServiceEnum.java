/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.fuyou.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike Date: 2017-03-30 15:12:06
 */
public enum FuyouServiceEnum implements Messageable {
    FIRST_QUICKPAY_CREATE_ORDER("firstQuickPayCreateOrder", "/apinewpay/orderAction", FuyouServiceTypeEnum.APIFMS, "SIGN", "快捷商户首次协议下单、验证码获取"),
    FIRST_AGREEMENT_PAY("firstAgreementPay", "/apinewpay/payAction", FuyouServiceTypeEnum.APIFMS, "SIGN", "首次协议支付"),
    FIRST_AGRPAY_RETCODE_ORDER("firstAgrpayRetCodeOrder", "/apinewpay/messageAction", FuyouServiceTypeEnum.APIFMS, "SIGN", "商户首次协议下单重发验证码"),
    AGRPAY_GETCODE_ORDER("agrpayGetCodeOrder", "/apipropay/orderAction", FuyouServiceTypeEnum.APIFMS, "SIGN", "协议下单验证码获取"),
    AGRPAY_RETCODE_ORDER("agrpayRetCodeOrder", "/apipropay/messageAction", FuyouServiceTypeEnum.APIFMS, "SIGN", "协议下单重发验证码"),
    AGREEMENT_PAY("agreementPay", "/apipropay/payAction", FuyouServiceTypeEnum.APIFMS, "SIGN", "协议支付"),
    AGREEMENT_UNBUNDLING("agreementUnbundling", "/cardPro/unbindAction", FuyouServiceTypeEnum.FMS, "SIGN", "协议解绑"),
    AGREEMENT_CARD_QUERY("agreementCardQuery", "/cardPro/queryAction", FuyouServiceTypeEnum.FMS, "SIGN", "协议卡查询"),
    FUYOU_ORDER_RESULT_QUERY("fuyouOrderResultQuery", "/findPay/queryOrderId", FuyouServiceTypeEnum.FM, "SIGN", "订单结果查询（富友订单号）"),
    MER_ORDER_RESULT_QUERY("merOrderResultQuery", "/checkInfo/checkResult", FuyouServiceTypeEnum.FM, "SIGN", "订单结果查询（商户订单号）"),
    UNPAID_ORDER_CLOSED("unpaidOrderClosed", "/mchntOrderOpr/disableOrder", FuyouServiceTypeEnum.FM, "SIGN", "未支付订单关闭"),
    MERSUPPORT_CARDBIN_QUERY("merSupportCardBinQuery", "/findPay/cardBinQuery", FuyouServiceTypeEnum.FM, "SIGN", "商户支持卡Bin查询"),
    QUOTA_QUERY("quotaQuery", "/amtlimit/lmtQueryAction", FuyouServiceTypeEnum.FM, "SIGN", "限额查询"),
    FUYOU_NETBANK("fuyouNetBank", "/smpGate", FuyouServiceTypeEnum.FM, "md5", "网银"),
    FUYOU_NETBANK_QUERY("fuyouNetBankQuery", "/smpQueryGate", FuyouServiceTypeEnum.STANDARD, "md5", "网银支付结果查询"),
    FUYOU_NETBANK_SYNCHRO_QUERY("fuyouNetBankSynchroQuery", "/smpAQueryGate", FuyouServiceTypeEnum.STANDARD, "md5", "网银支付结果同步查询"),
    FUYOU_WITHDRAW("fuyouWithdraw", "fuyouWithdraw", FuyouServiceTypeEnum.WI, "mac", "代付"),
    FUYOU_WITHDRAW_QUERY("fuyouWithdrawQuery", "fuyouWithdrawQuery", FuyouServiceTypeEnum.WI, "mac", "代付查询"),
    FUYOU_BIND_MSG("fuyouBindMsg", "/newpropay/bindMsg", FuyouServiceTypeEnum.APIFMS, "SIGN", "协议卡绑定发送短信验证码1.3"),
    FUYOU_BIND_COMMIT("fuyouBindCommit", "/newpropay/bindCommit", FuyouServiceTypeEnum.APIFMS, "SIGN", "协议卡绑定1.3"),
    FUYOU_UN_BIND("fuyouUnBind", "/newpropay/unbind", FuyouServiceTypeEnum.APIFMS, "SIGN", "协议解绑1.3"),
    FUYOU_BIND_QUERY("fuyouBindQuery", "/newpropay/bindQuery", FuyouServiceTypeEnum.APIFMS, "SIGN", "协议卡查询1.3"),
    FUYOU_ORDER_PAY("fuyouOrderPay", "/newpropay/order", FuyouServiceTypeEnum.APIFMS, "SIGN", "协议卡支付1.3"),

    ;

    private final String code;
    private final String key;
    private final FuyouServiceTypeEnum type;
    private final String signKey;
    private final String message;

    private FuyouServiceEnum(String code, String key, FuyouServiceTypeEnum type, String signKey, String message) {
        this.code = code;
        this.message = message;
        this.key = key;
        this.signKey = signKey;
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public String getKey() {
        return key;
    }

    public String getSignKey() {
        return signKey;
    }

    public FuyouServiceTypeEnum getType() {
        return type;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (FuyouServiceEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static FuyouServiceEnum find(String code) {
        for (FuyouServiceEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param key 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 key 没有对应的 Status 。
     */
    public static FuyouServiceEnum findByKey(String key) {
        for (FuyouServiceEnum status : values()) {
            if (status.getKey().equals(key)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param type 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 key 没有对应的 Status 。
     */
    public static FuyouServiceEnum findByType(FuyouServiceTypeEnum type) {
        for (FuyouServiceEnum status : values()) {
            if (status.getType().equals(type)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<FuyouServiceEnum> getAll() {
        List<FuyouServiceEnum> list = new ArrayList<FuyouServiceEnum>();
        for (FuyouServiceEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (FuyouServiceEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }
}
