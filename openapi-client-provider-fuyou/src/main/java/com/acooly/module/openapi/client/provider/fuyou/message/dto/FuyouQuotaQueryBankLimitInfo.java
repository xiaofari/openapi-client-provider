package com.acooly.module.openapi.client.provider.fuyou.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/3/21 15:21
 */
@Getter
@Setter
@XStreamAlias("BANKLMT")
public class FuyouQuotaQueryBankLimitInfo implements Serializable{

    /**
     * 银行机构号
     * 支付的银行机构号
     */
    @XStreamAlias("INSCD")
    @NotBlank
    @Size(max = 20)
    private String insCd;

    /**
     * 卡类型
     * 返回的卡类型
     */
    @XStreamAlias("CARDTYPE")
    private String cardType;

    /**
     * 单笔限额(最高)
     * 商户交易单笔限额(分)
     */
    @XStreamAlias("AMTLIMITTIME")
    private String amtLimitTime;

    /**
     * 单笔限额(最低)
     * 商户交易单笔最低限额(分)
     */
    @XStreamAlias("AMTLIMITTIMELOW")
    private String amtLimitTimeLow;

    /**
     * 单日限额
     * 商户交易当日限额(分)
     */
    @XStreamAlias("AMTLIMITDAY")
    private String amtLimitDay;

    /**
     * 单月限额
     * 商户交易当月限额(分)
     */
    @XStreamAlias("AMTLIMITMONTH")
    private String amtLimitMonth;
}
