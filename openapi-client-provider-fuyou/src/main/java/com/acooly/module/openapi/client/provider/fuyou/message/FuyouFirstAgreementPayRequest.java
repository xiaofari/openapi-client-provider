package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:04
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FIRST_AGREEMENT_PAY,type = ApiMessageType.Request)
@XStreamAlias("REQUEST")
public class FuyouFirstAgreementPayRequest extends FuyouRequest {

    /**
     * 客户 IP
     * 客户所在 IP 地址
     */
    @XStreamAlias("USERIP")
    @NotBlank
    private String userIp;

    /**
     * 交易类型
     */
    @XStreamAlias("TYPE")
    @NotBlank
    private String type = "03";

    /**
     * 商户订单号
     * 商户订单流水号商户确保唯一
     */
    @Size(max = 60)
    @NotBlank
    @XStreamAlias("MCHNTORDERID")
    private String merchOrderNo;

    /**
     * 用户编号
     * 商户端用户的唯一编号，即用户 ID
     */
    @XStreamAlias("USERID")
    @NotBlank
    @Size(max = 40)
    private String userId;

    /**
     * 富友订单号
     * 富友生成的订单号，该订单号在相
     * 当长的时间内不重复。富友通过订
     * 单号来唯一确认一笔订单的重复性
     */
    @Size(max = 4)
    @XStreamAlias("ORDERID")
    @NotBlank
    private String orderId;

    /**
     * 银行卡号
     */
    @XStreamAlias("BANKCARD")
    @NotBlank
    @Size(max = 20)
    private String bankCard;

    /**
     * 银行预留手
     *身银行预留手机号
     */
    @XStreamAlias("MOBILE")
    @NotBlank
    @Size(max = 11)
    private String mobileNo;

    /**
     * 短信验证码
     */
    @XStreamAlias("VERCD")
    @NotBlank
    @Size(max = 20)
    private String vercd;

    /**
     * 保留字段 1
     */
    @XStreamAlias("REM1")
    @NotBlank
    @Size(max = 256)
    private String remOne;

    /**
     * 保留字段 2
     */
    @XStreamAlias("REM2")
    @NotBlank
    @Size(max = 256)
    private String remTwo;

    /**
     * 保留字段 3
     */
    @XStreamAlias("REM3")
    @NotBlank
    @Size(max = 256)
    private String remThree;

    /**
     * 下单验证码
     * 接口返回的
     * SIGNPAY 字
     * 段
     * 原样传送下单验证码接口返回的
     * SIGNPAY 字段
     */
    @XStreamAlias("SIGNPAY")
    @NotBlank
    private String signPay;

    @Override
    public String getSignStr() {
        return getType()+"|"+getVersion()+"|"+getPartner()+"|"+getOrderId()+"|"+getMerchOrderNo()+"|"+getUserId()+"|"+getBankCard()+"|"+getVercd()
                +"|"+getMobileNo()+"|"+getUserIp()+"|";
    }
}
