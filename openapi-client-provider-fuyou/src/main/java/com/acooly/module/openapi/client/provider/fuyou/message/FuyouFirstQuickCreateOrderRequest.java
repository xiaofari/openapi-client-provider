package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:04
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FIRST_QUICKPAY_CREATE_ORDER,type = ApiMessageType.Request)
@XStreamAlias("REQUEST")
public class FuyouFirstQuickCreateOrderRequest extends FuyouRequest {

    /**
     * 客户 IP
     * 客户所在 IP 地址
     */
    @XStreamAlias("USERIP")
    @NotBlank
    private String userIp;

    /**
     * 交易类型
     */
    @XStreamAlias("TYPE")
    @NotBlank
    private String type = "03";

    /**
     * 商户订单号
     * 商户订单流水号商户确保唯一
     */
    @Size(max = 60)
    @NotBlank
    @XStreamAlias("MCHNTORDERID")
    private String merchOrderNo;

    /**
     * 订单存活期
     * 订单存活期(单位：分钟)，不填默
     * 认 24 小时后订单未支付则过期。
     */
    @Size(max = 4)
    @XStreamAlias("ORDERALIVETIME")
    private String orderAliveTime;

    /**
     * 用户编号
     * 商户端用户的唯一编号，即用户 ID
     */
    @XStreamAlias("USERID")
    @NotBlank
    @Size(max = 40)
    private String userId;

    /**
     * 交易金额
     * 交易金额，分为单位
     */
    @XStreamAlias("AMT")
    @NotBlank
    @Size(max = 12)
    private String amount;


    /**
     * 银行卡号
     */
    @XStreamAlias("BANKCARD")
    @NotBlank
    @Size(max = 20)
    private String bankCard;

    /**
     * 真实姓名
     */
    @XStreamAlias("NAME")
    @NotBlank
    @Size(max = 20)
    private String reaNname;

    /**
     * 证件类型
     * 0.身份证
     */
    @XStreamAlias("IDTYPE")
    @NotBlank
    @Size(max = 1)
    private String idType = "0";

    /**
     * 证件号
     *身用户证件号，此处填写身份证号
     */
    @XStreamAlias("IDNO")
    @NotBlank
    @Size(max = 20)
    private String idNumber;

    /**
     * 银行预留手
     *身银行预留手机号
     */
    @XStreamAlias("MOBILE")
    @NotBlank
    @Size(max = 11)
    private String mobileNo;

    /**
     * CVN
     *信用卡的 3 位 Cvn、4 位 ValidDate
     *字段，组合在一起（变成 8 位的
     *Cvn+”;”+ValidDate）进行加密后
     *放在 CVN 字段传值
     *信用卡必填
     */
    @XStreamAlias("CVN")
    private String cvn;

    /**
     * 后台通知URL
     */
    @XStreamAlias("BACKURL")
    @NotBlank
    @Size(max = 200)
    private String notifyUrl;

    /**
     * 保留字段 1
     */
    @XStreamAlias("REM1")
    @NotBlank
    @Size(max = 256)
    private String remOne;

    /**
     * 保留字段 2
     */
    @XStreamAlias("REM2")
    @NotBlank
    @Size(max = 256)
    private String remTwo;

    /**
     * 保留字段 3
     */
    @XStreamAlias("REM3")
    @NotBlank
    @Size(max = 256)
    private String remThree;

    @Override
    public String getSignStr() {
        return getType()+"|"+getVersion()+"|"+getPartner()+"|"+getMerchOrderNo()+"|"+getUserId()+"|"+getAmount()+"|"+getBankCard()+"|"+getNotifyUrl()
                +"|"+getReaNname()+"|"+getIdNumber()+"|"+getIdType()+"|"+getMobileNo()+"|"+getUserIp()+"|";
    }
}
