package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:04
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.AGRPAY_RETCODE_ORDER,type = ApiMessageType.Request)
@XStreamAlias("REQUEST")
public class FuyouAgrpayRetCodeOrderRequest extends FuyouRequest{

    /**
     * 客户 IP
     * 客户所在 IP 地址
     */
    @XStreamAlias("USERIP")
    @NotBlank
    private String userIp;

    /**
     * 交易类型
     */
    @XStreamAlias("TYPE")
    @NotBlank
    private String type = "03";

    /**
     * 富友订单号
     * 富友生成的订单号，该订单号在相
     当长的时间内不重复。富友通过订
     单号来唯一确认一笔订单的重复性
     */
    @XStreamAlias("ORDERID")
    private String bankOrderId;

    /**
     * 用户编号
     * 商户端用户的唯一编号，即用户 ID
     */
    @XStreamAlias("USERID")
    @NotBlank
    @Size(max = 40)
    private String userId;

    /**
     * 交易金额
     * 交易金额，分为单位
     */
    @XStreamAlias("AMT")
    @NotBlank
    @Size(max = 12)
    private String amount;

    /**
     * 协议号
     * 首次协议交易成功好生成的协议号
     */
    @XStreamAlias("PROTOCOLNO")
    @NotBlank
    @Size(max = 30)
    private String protocolNo;

    /**
     * 后台通知URL
     */
    @XStreamAlias("BACKURL")
    @NotBlank
    @Size(max = 200)
    private String notifyUrl;

    /**
     * 保留字段 1
     */
    @XStreamAlias("REM1")
    @NotBlank
    @Size(max = 256)
    private String remOne;

    /**
     * 保留字段 2
     */
    @XStreamAlias("REM2")
    @NotBlank
    @Size(max = 256)
    private String remTwo;

    /**
     * 保留字段 3
     */
    @XStreamAlias("REM3")
    @NotBlank
    @Size(max = 256)
    private String remThree;

    @Override
    public String getSignStr() {
        return getType()+"|"+getVersion()+"|"+getPartner()+"|"+getBankOrderId()+"|"+getUserId()+"|"+getProtocolNo()+"|"+getAmount()+"|"+getNotifyUrl()
                +"|"+getUserIp()+"|";
    }
}
