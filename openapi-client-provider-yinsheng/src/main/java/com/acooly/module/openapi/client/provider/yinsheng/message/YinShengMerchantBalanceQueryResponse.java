package com.acooly.module.openapi.client.provider.yinsheng.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengResponse;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import com.acooly.module.openapi.client.provider.yinsheng.message.dto.YinShengAccountDetailInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author zhike 2018/2/22 9:58
 */
@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.MERCHANT_BALANCE_QUERY, type = ApiMessageType.Response)
public class YinShengMerchantBalanceQueryResponse extends YinShengResponse{

    /**
     * 账户总额
     */
    private String account_total_amount;

    /**
     * 账户明细信息
     */
    private List<YinShengAccountDetailInfo> account_detail;
}
