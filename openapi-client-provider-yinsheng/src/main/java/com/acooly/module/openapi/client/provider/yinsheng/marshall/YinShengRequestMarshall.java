/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.yinsheng.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengRequest;
import com.acooly.module.openapi.client.provider.yinsheng.utils.SignUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author zhangpu
 */
@Service
public class YinShengRequestMarshall extends YinShengMarshallSupport implements ApiMarshal<String, YinShengRequest> {

    private static final Logger logger = LoggerFactory.getLogger(YinShengRequestMarshall.class);

    @Override
    public String marshal(YinShengRequest source) {
        beforeMarshall(source);
        Map<String, String> requestDataMap = doMarshall(source);
        StringBuilder requestContent = new StringBuilder();
        SignUtils.buildPayParams(requestContent,requestDataMap,false);
        return requestContent.toString();
    }

}
