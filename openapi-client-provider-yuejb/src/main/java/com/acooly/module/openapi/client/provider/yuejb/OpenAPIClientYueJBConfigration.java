package com.acooly.module.openapi.client.provider.yuejb;

import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import static com.acooly.module.openapi.client.provider.yuejb.OpenAPIClientYueJBProperties.PREFIX;

/**
 * Created by ouwen@yiji.com} on 2017/11/6.
 */

@EnableConfigurationProperties({OpenAPIClientYueJBProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class OpenAPIClientYueJBConfigration {

    @Autowired
    private OpenAPIClientYueJBProperties openAPIClientYueJBProperties;

    @Bean("yuejbHttpTransport")
    public Transport YueJBHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(openAPIClientYueJBProperties.getGateway());
        httpTransport.setConnTimeout(String.valueOf(openAPIClientYueJBProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientYueJBProperties.getReadTimeout()));
        httpTransport.setContentType(ContentType.APPLICATION_FORM_URLENCODED.getMimeType());
        return httpTransport;
    }

}
