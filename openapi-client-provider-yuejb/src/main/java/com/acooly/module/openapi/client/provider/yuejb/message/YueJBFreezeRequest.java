package com.acooly.module.openapi.client.provider.yuejb.message;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.yuejb.domain.YueJBRequest;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by ouwen@yiji.com} on 2017/11/6.
 */
@Data
public class YueJBFreezeRequest extends YueJBRequest {
    /**
     * 冻结金额
     */
    @MoneyConstraint(min = 1L)
    private Money amount;

    /**
     * 产品编号
     */
    @NotEmpty
    private String productNo;

    /**
     * 外部会员ID
     */
    @NotEmpty
    private String storeId;

    /**
     * 供应商标识
     */
    @NotEmpty
    private String markCode;

    /**
     * 备注
     */
    private String memo;
}
