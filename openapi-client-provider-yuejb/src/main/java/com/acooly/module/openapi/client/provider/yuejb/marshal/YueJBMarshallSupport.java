/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月4日
 *
 */
package com.acooly.module.openapi.client.provider.yuejb.marshal;

import com.acooly.module.openapi.client.provider.yuejb.OpenAPIClientYueJBProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ouwen
 */
@Slf4j
@Service
public class YueJBMarshallSupport {

    @Autowired
    protected OpenAPIClientYueJBProperties openAPIClientYueJBProperties;


    protected OpenAPIClientYueJBProperties getProperties() {
        return openAPIClientYueJBProperties;
    }

}
