/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.yibao.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author zhangpu
 * @date 2018-1-23
 */
@Slf4j
@Component
public class YibaoResponseUnmarshall extends YibaoAbstractMarshall implements ApiUnmarshal<YibaoResponse, String> {


    @SuppressWarnings("unchecked")
    @Override
    public YibaoResponse unmarshal(String message, String serviceName) {
        return doUnMarshall(message, serviceName, false);
    }


}
