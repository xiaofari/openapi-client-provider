package com.acooly.module.openapi.client.provider.yibao.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoAlias;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoApiMsg;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoRequest;
import com.acooly.module.openapi.client.provider.yibao.enums.YibaoServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@YibaoApiMsg(service = YibaoServiceNameEnum.YIBAO_BINDPAY_RECORD,type = ApiMessageType.Request)
public class YibaoRechargeOrderQueryRequest extends YibaoRequest {

    /**
     * 银行流水号
     */
    @YibaoAlias(value = "yborderid")
    private String bankOrderNo;
}
