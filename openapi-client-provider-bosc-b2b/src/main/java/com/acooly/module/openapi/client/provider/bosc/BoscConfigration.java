package com.acooly.module.openapi.client.provider.bosc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;

import static com.acooly.module.openapi.client.provider.bosc.BoscProperties.PREFIX;

@EnableConfigurationProperties({ BoscProperties.class })
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class BoscConfigration {

	@Autowired
	private BoscProperties boscProperties;

	@Bean("boscb2bHttpTransport")
	public Transport boscHttpTransport() {
		HttpTransport httpTransport = new HttpTransport();
		httpTransport.setGateway(boscProperties.getRequestUrl());
		httpTransport.setConnTimeout(boscProperties.getConnTimeout());
		httpTransport.setReadTimeout(boscProperties.getReadTimeout());
		httpTransport.setCharset("utf-8");
		httpTransport.setContentType("text/xml");
		return httpTransport;
	}

}
